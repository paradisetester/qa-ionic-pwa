import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { EmailComposer } from '@ionic-native/email-composer';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ThankyouPage } from '../pages/thankyou/thankyou';

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuth } from 'angularfire2/auth';
import { firebaseConfig } from '../config';
import { AuthService } from '../services/auth.service';
import { NgxErrorsModule } from '@ultimate/ngxerrors';
 
import { RestProvider } from '../providers/rest/rest';
import { LocationStrategy, PathLocationStrategy, APP_BASE_HREF } from '@angular/common';

@NgModule({
  declarations: [
    MyApp,
    HomePage,	
    ThankyouPage,	
  ],
  imports: [
	NgxErrorsModule,
    BrowserModule,
    IonicModule.forRoot(MyApp, {}, {
		 links: [
		  { component: HomePage, name: 'home', segment: 'home' },
		  { component: ThankyouPage, name: 'thankyou', segment: 'thankyou' },
		 ]
	  }),
	AngularFireModule.initializeApp(firebaseConfig.fire),	
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,	 
    ThankyouPage,	 
  ],
  providers: [
    StatusBar,
		EmailComposer,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
	AngularFireAuth,
	AuthService,
    RestProvider,
	{provide: LocationStrategy, useClass: PathLocationStrategy}
  ]
})
export class AppModule {}
