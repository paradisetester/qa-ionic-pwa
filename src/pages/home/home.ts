import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ThankyouPage } from '../thankyou/thankyou';
import { AuthService } from '../../services/auth.service';
import $ from 'jquery';
import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})


export class HomePage {
	skeletion=true;
	section_web=false;
	responseMsg: any;
  constructor(private navCtrl: NavController, private auth: AuthService) {
	
			 
			
	}
	
	 public encrypted_function(token){
				var key = CryptoJS.enc.Utf8.parse('7061737323313233');
				var iv = CryptoJS.enc.Utf8.parse('7061737323313233');		
			  
		var encrypted = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(token.toString()), key,
				{
					keySize: 128 / 8,
					iv: iv,
					mode: CryptoJS.mode.CBC,
					padding: CryptoJS.pad.Pkcs7
				});
		return encrypted.toString();
		
		}
		
	public makepassword(l)
			{
				var text = "";
				var char_list = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
				for(var i=0; i < l; i++ )
				{  
					text += char_list.charAt(Math.floor(Math.random() * char_list.length));
				}
				return text;
			}
			
public sendMail(){
	 this.responseMsg = "processing...";
	  var pass = this.makepassword(8);			 	
	  var encrypt = this.encrypted_function(pass);
	 console.log(pass+' -- '+encrypt);
			 
	 var email =  $('#mc-embedded-subscribe-form #email').val();
	 if(email){
		 $.ajax({
				 type: "post",
				 url:  "https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8",
				 data: $('#mc-embedded-subscribe-form').serialize(),
				 success: function() {
						this.navCtrl.setRoot(ThankyouPage);
				 },
				 error: function(req) {
					return this.responseMsg = "Subscribed Failed.";
					
				 }
			 });	
			
			 
			  
			 $.ajax({
				 type: "post",
				 url:  "https://ownsatta.com/api/email",
				 data: {email:email, password:pass, token:encrypt},
				 success: function() {
				 },
				 error: function(req) {
					return this.responseMsg = "Subscribed Failed.";
					
				 }
			 });
			 
			 
			 let credentials = {
				email: email,
				password: encrypt
			   };
			   this.auth.signUp(credentials).then(	
				() => this.navCtrl.setRoot(ThankyouPage),
				error =>  error.message			
			   );
			   		
			 this.navCtrl.setRoot(ThankyouPage); 
				 
		
	 }else{
		return this.responseMsg = "Please Enter your email ID.";
	 }	
		 return false;
}





	ionViewDidLoad() {
		let TIME_IN_MS = 500;
		let hideFooterTimeout = setTimeout( () => {
			this.section_web = true;
			this.skeletion = false;
			
		}, TIME_IN_MS);
		
	}
	
}


