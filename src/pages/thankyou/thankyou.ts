import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
/**
 * Generated class for the ThankyouPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-thankyou',
  templateUrl: 'thankyou.html',
})
export class ThankyouPage {
	email: string;
	token: string;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  this.email = navParams.get('email');
  this.token = navParams.get('token');
  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ThankyouPage');
  }

	homePagePush(){
		this.navCtrl.push(HomePage);
	}
}
